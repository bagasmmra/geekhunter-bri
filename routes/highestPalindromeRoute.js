import express from 'express';
import {getHighestPalindrome} from "../controllers/highestPalindromeController.js";

const router = express.Router();

router.post('/', getHighestPalindrome);

export default router;
