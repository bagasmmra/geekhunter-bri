import express from 'express';
import {checkBracket} from "../controllers/balancedBracketController.js";

const router = express.Router();

router.post('/', checkBracket);

export default router;
