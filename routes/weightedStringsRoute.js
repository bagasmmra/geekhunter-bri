import express from 'express';
import { getWeightedStrings } from '../controllers/weightedStringsController.js';

const router = express.Router();

router.post('/', getWeightedStrings);

export default router;
