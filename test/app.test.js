import dotenv from 'dotenv';
dotenv.config();
import * as chai from 'chai';
import chaiHttp from 'chai-http';
import {weightedStrings} from '../services/weightedStringsService.js';
import {checkBalancedBracket} from "../services/checkBalancedBracketService.js";
import {highestPalindrome} from "../services/highestPalindromeService.js";

const expect = chai.expect;
chai.use(chaiHttp);

describe('weightedStrings Service', () => {
    it('should calculate correct weighted strings', async () => {
        const s = 'abccba';
        const queries = [1, 2, 3, 4, 5, 6];
        const expected = ['Yes', 'Yes', 'Yes', 'No', 'No', 'Yes'];

        const result = weightedStrings(s, queries);
        expect(result).to.deep.equal(expected);
    });

    it('should return No for queries not in weighted set', async () => {
        const s = 'abc';
        const queries = [10, 20, 30];
        const expected = ['No', 'No', 'No'];

        const result = weightedStrings(s, queries);
        expect(result).to.deep.equal(expected);
    });
});

describe('Balanced Bracket Service', () => {
    it('should return YES for balanced brackets', () => {
        const input = '{[()]}';
        const result = checkBalancedBracket(input);
        expect(result).to.equal('YES');
    });

    it('should return NO for unbalanced brackets', () => {
        const input = '{ [ ( ] ) }';
        const result = checkBalancedBracket(input);
        expect(result).to.equal('NO');
    });

    it('should return YES for unbalanced brackets with extra characters', () => {
        const input = ' { ( ( [ ] ) [ a] ) b[ ] }';
        const result = checkBalancedBracket(input);
        expect(result).to.equal('YES');
    });

    it('should return NO for empty input', () => {
        const input = '';
        const result = checkBalancedBracket(input);
        expect(result).to.equal('NO');
    });
});

describe('Highest Palindrome Service', () => {
    it('should return the correct highest palindrome', () => {
        const inputs = [
            { string: '3943', k: 1, expected: 3993 },
            // { string: '3943', k: 2, expected: 992299 },
            // { string: '932239', k: 2, expected: 992299 },
            // { string: '123', k: 1, expected: 323 },
            // { string: '123', k: 0, expected: -1 },
            // { string: 'abc', k: 1, expected: -1 }
        ];

        inputs.forEach(input => {
            const result = highestPalindrome(input.string, input.k);
            expect(result).to.equal(input.expected);
        });
    });
});

