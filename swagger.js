import swaggerJsdoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';

const options = {
    definition: {
        openapi: '3.0.0',
        info: {
            title: 'Your Express API',
            version: '1.0.0',
            description: 'API documentation for your Express application',
        },
    },
    // Path to the API docs
    apis: ['./router.js'], // Path to your router file
};

const specs = swaggerJsdoc(options);

export default specs;
