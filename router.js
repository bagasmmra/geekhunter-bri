/**
 * @swagger
 * tags:
 *   name: Weighted Strings
 *   description: API endpoints for weighted strings operations
 */

/**
 * @swagger
 * tags:
 *   name: Highest Palindrome
 *   description: API endpoints for finding the highest palindrome
 */

/**
 * @swagger
 * tags:
 *   name: Check Balanced Bracket
 *   description: API endpoints for checking balanced brackets
 */

import express from 'express';
import weightedStringsRoute from './routes/weightedStringsRoute.js';
import highestPalindromeRoute from './routes/highestPalindromeRoute.js';
import checkBalancedBracketRoute from './routes/checkBalancedBracketRoute.js';

const router = express.Router();

/**
 * @swagger
 * /weighted-strings:
 *   post:
 *     summary: Get weighted strings
 *     tags: [Weighted Strings]
 *     responses:
 *       200:
 *         description: Returns the weighted strings
 */
router.use('/weighted-strings', weightedStringsRoute);

/**
 * @swagger
 * /highest-palindrome:
 *   post:
 *     summary: Get highest palindrome
 *     tags: [Highest Palindrome]
 *     responses:
 *       200:
 *         description: Returns the highest palindrome
 */
router.use('/highest-palindrome', highestPalindromeRoute);

/**
 * @swagger
 * /check:
 *   post:
 *     summary: Check if brackets are balanced
 *     tags: [Check Balanced Bracket]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               expression:
 *                 type: string
 *           example:
 *             expression: "{[()]}"
 *     responses:
 *       200:
 *         description: Returns true if brackets are balanced, false otherwise
 */
router.use('/check', checkBalancedBracketRoute);

export default router;
