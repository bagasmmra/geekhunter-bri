# Express.js Project

This is a simple Express.js project showcasing how to create a basic web application using Express.

## Installation

To install the project and its dependencies, run the following commands:

```bash
git clone https://gitlab.com/bagasmmra/geekhunter-bri.git
cd geekhunter-bri
npm install
npm test