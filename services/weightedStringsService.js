export function weightedStrings(s, queries) {
    const weights = new Set();
    let prevChar = '';
    let count = 0;

    for (let i = 0; i < s.length; i++) {
        const char = s[i];
        const charWeight = char.charCodeAt(0) - 'a'.charCodeAt(0) + 1;

        if (char !== prevChar) {
            prevChar = char;
            count = 1;
        } else {
            count++;
        }

        for (let j = 1; j <= count; j++) {
            weights.add(charWeight * j);
        }
    }

    return queries.map(query => weights.has(query) ? 'Yes' : 'No');
}
