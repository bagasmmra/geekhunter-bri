export function checkBalancedBracket(input) {
    if (input.length === 0) {
        return 'NO';
    }

    const stack = [];
    const openingBrackets = ['(', '[', '{'];
    const closingBrackets = [')', ']', '}'];

    for (let char of input) {
        if (openingBrackets.includes(char)) {
            stack.push(char);
        } else if (closingBrackets.includes(char)) {
            const lastOpeningBracket = stack.pop();
            const correspondingOpeningBracket = openingBrackets[closingBrackets.indexOf(char)];
            if (lastOpeningBracket !== correspondingOpeningBracket) {
                return 'NO';
            }
        }
    }

    if (stack.length !== 0) {
        return 'NO';
    }

    return 'YES';
}