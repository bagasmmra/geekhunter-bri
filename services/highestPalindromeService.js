export function highestPalindrome(string, k) {
    const arr = string.split('');
    let i = 0;
    let j = arr.length - 1;
    let changes = 0; // Jumlah perubahan yang telah dilakukan

    // Loop untuk membandingkan karakter pertama dan terakhir
    while (i < j) {
        // Jika karakter tidak sama
        if (arr[i] !== arr[j]) {
            // Ganti dengan yang lebih besar
            arr[i] = arr[j] = Math.max(arr[i], arr[j]);
            k--; // Kurangi jumlah karakter yang dapat diganti
            changes++; // Tandai perubahan karakter
        }
        // Pindahkan pointer
        i++;
        j--;
    }

    // Jika jumlah perubahan melebihi jumlah kesempatan
    if (k < 0) {
        return -1;
    }

    // Jika string sudah menjadi palindrom, tapi masih ada kesempatan penggantian, ubah karakter tengah menjadi '9'
    if (i === j && k > 0) {
        arr[i] = '9';
    }

    return parseInt(arr.join(''));
}
