import { weightedStrings } from '../services/weightedStringsService.js';

export function getWeightedStrings(req, res) {
    const { s, queries } = req.body;
    if (!s || !Array.isArray(queries)) {
        return res.status(400).send('Invalid input');
    }

    const result = weightedStrings(s, queries);
    res.json(result);
}
