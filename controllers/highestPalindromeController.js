import {highestPalindrome} from "../services/highestPalindromeService.js";

export function getHighestPalindrome(req, res) {
    const { string, k } = req.query;
    if (!string || !k) {
        return res.status(400).json({ error: 'String and k parameter are required.' });
    }

    const result = highestPalindrome(string, parseInt(k));
    if (result === -1) {
        return res.status(400).json({ error: 'Cannot find a palindrome.' });
    }

    return res.json({ highestPalindrome: result });
}