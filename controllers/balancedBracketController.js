import {checkBalancedBracket} from "../services/checkBalancedBracketService.js";

export function checkBracket(req, res) {
    const input = req.body.input;
    const result = checkBalancedBracket(input);
    res.json({ result: result });
}